<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\User;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Email or Password not match !'], 401);
        } else if ($user->email_verified_at == null) {
            return response()->json([
                'error' => 'Email Not Verified, Please Verify Your Email First !',
            ], 401);
        } else {
            $data['token'] = $token;
            $data['user'] = auth()->user();
            return response()->json([
                'response_code' => '00',
                'response_message' => 'Login Success!',
                'data' => $data,
            ]);
        }
    }
}
