<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegenerateOtpEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegenerateOtpRequest;
use App\Mail\RegenerateOtpMail;
use App\Otp_Code;
use App\User;
use Carbon\Carbon;
use Mail;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegenerateOtpRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email Not Registered',
            ], 200);
        } else {
            $now = Carbon::now();
            // add 5 mins to the current time
            $otpExpires = $now->addMinutes(5);
            $code = mt_rand(100000, 999999);

            $otp = Otp_Code::updateOrCreate(
                ['user_id' => $user->id],
                ['code' => $code, 'valid_until' => $otpExpires]
            );

            event(new RegenerateOtpEvent($user));

            return response()->json([
                'response_code' => '00',
                'response_message' => 'OTP Code Has Been Sent to Your Email!',
                'data' => $otp,
            ]);
        }
    }
}
