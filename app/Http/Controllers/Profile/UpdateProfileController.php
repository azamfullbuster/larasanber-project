<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdateProfileController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth:api');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'image' => 'mimes:png,jpg,jpeg',
        ]);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension();
            $filename = $request->user()->id . '.' . $extension;
            $image_folder = 'uploads/profpic/';
            $image_location = $image_folder . $filename;

            try {
                $file->move(public_path($image_folder), $filename);

                $request->user()->name = $request->name;
                $request->user()->photo = $image_location;
                $request->user()->update();

                return response()->json([
                    'response_code' => '00',
                    'response_message' => 'Profile Updated',
                    'data' => $request->user(),
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Profile picture failed to upload',
                    'data' => $data
                ], 200);
            }
        } else {
            /* return $request; */
            $request->user()->name = $request->name;
            $request->user()->update();

            return response()->json([
                'response_code' => '00',
                'response_message' => 'Profile Updated',
                'data' => $request->user(),
            ]);
        }
    }
}
