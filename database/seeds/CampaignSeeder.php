<?php

use Illuminate\Database\Seeder;

class CampaignSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaigns = [];
        $faker = Faker\Factory::create('id_ID');
        for ($i = 0; $i < 15; $i++) {
            $img_folder = '/photos/campaign/';
            $full_path = $faker->image(public_path($img_folder), 640, 480, 'city');
            $img = explode("\\", $full_path);
            $campaigns[$i] = [
                'id' => $faker->uuid,
                'title' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                'description' => $faker->text,
                'address' => $faker->address,
                'required' => mt_rand(100000, 50000000),
                'collected' => mt_rand(10000, 100000),
                'image' => $img_folder . $img[6],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        DB::table('campaigns')->insert($campaigns);
    }
}
