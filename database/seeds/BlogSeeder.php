<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogs = [];
        $faker = Faker\Factory::create('id_ID');
        for ($i = 0; $i < 15; $i++) {
            $img_folder = '/photos/blog/';
            $full_path = $faker->image(public_path($img_folder), 640, 480, $category = 'food');
            $img = explode("\\", $full_path);
            $blogs[$i] = [
                'id' => $faker->uuid,
                'title' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'description' => $faker->text,
                'image' => $img_folder . $img[6],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        DB::table('blogs')->insert($blogs);
    }
}
